/*
A palindromic number reads the same both ways. The largest palindrome made from the product of two 2-digit numbers is 9009 = 91 × 99.
Find the largest palindrome made from the product of two 3-digit numbers.
*/

const cache = {};

const isPalindrome = (possiblePalindrome) => {
  // We could convert the possble palindrome to a string and compare the left and right in a loop,
  // but no, we want to leave it as a number and do it the hard way.
  // We calulate the largest base 10 and divide it in half to get the maximum
  // number of times we need to loop.
  const largestBase10 = Math.floor(Math.log10(possiblePalindrome));
  const max = Math.floor(largestBase10 / 2);
  let left = 0;
  let right = 0;
  for (let i3 = 0; i3 <= max; i3 += 1) {
    // If possible palindrome was 364, then larbest base 10 is 2.
    // 364 / (10 pow 2) = 364 / 100 = 3.64. Floor 3.64 is 3. 3 mod 10 is 3.
    left = Math.floor(possiblePalindrome / Math.pow(10, largestBase10 - i3)) % 10;
    // 364 / (10 pow 0) = 364 / 1 = 364. Floor 364 is 364. 364 % 10 is 4.
    right = Math.floor(possiblePalindrome / Math.pow(10, i3)) % 10;
    if (left !== right) {
      return false;
    }
  }
  return true;
}

const problem4 = (int1, int2) => {
  let returnValue = -1;
  let count = 0;
  const pad1 = Math.ceil(Math.log10(int1));
  const pad2 = Math.ceil(Math.log10(int2));
  for (let i1 = int1; i1 >= 1; i1 -= 1) {
    for (let i2 = int2; i2 >= 1; i2 -= 1) {
      const possiblePalindrome = i1 * i2;

      // Using int1 and int2, we add a entry in the cache of their possible palindrome.
      // Eg. if int1 was 52 and int2 was 7, we store their product at:
      // cache['052007'] = 364;
      // cache['007052'] = 364;
      // Wished we have sprintf in JS but we don't. =()
      // So we have to pad the integer using log10.
      let i1Padded = i1 + '';
      let i2Padded = i2 + '';
      while (i1Padded.length < pad1) i1Padded = '0' + i1Padded + ''; 
      while (i2Padded.length < pad2) i2Padded = '0' + i2Padded + ''; 
      
      // If we have a cache hit, then skip skip.
      if (cache['' + i1Padded + i2Padded + '']) {
        // console.log('' + i1Padded + i2Padded + '');
        continue;
      }
      cache['' + i1Padded + i2Padded + ''] = possiblePalindrome;
      cache['' + i2Padded + i1Padded + ''] = possiblePalindrome;
      count++; // See how many times it looped
      if (isPalindrome(possiblePalindrome)) {
        if (possiblePalindrome > returnValue) {
          returnValue = possiblePalindrome;
        }
      }
    }
  }
  // console.log(count);
  return returnValue;
}

console.log(problem4(999, 999));