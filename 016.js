/*
215 = 32768 and the sum of its digits is 3 + 2 + 7 + 6 + 8 = 26.

What is the sum of the digits of the number 21000?
*/

const doubleString = (stringNumber) => {
  // console.log('=======');
  let returnValue = [];
  let ten = 0;
  for (let i = stringNumber.length - 1; i >= 0; i -= 1) {
    let digit = stringNumber[i];
    digit = Number(digit) * 2;
    if (ten) {
      digit += ten;
      ten = 0;
    }
    if (digit >= 10) {
      ten = 1;
      digit = (digit + '').substr(1, 1);
    }
    digit = digit + '';
    returnValue.splice(0, 0, digit);
  }

  if (ten) {
    returnValue.splice(0, 0, '1');
  }

  returnValue = returnValue.join('')
  // console.log(returnValue);

  return returnValue;
}

const getSumOfEachDigit = (stringNumber) => {
  let returnValue = 0;
  for(i = 0; i < stringNumber.length; i += 1) {
    returnValue += Number(stringNumber[i]);
  }
  return returnValue;
}

const problem16 = (pow) => {
  let stringNumber = '2';
  for (let n = 1; n < pow; n += 1) {
    stringNumber = doubleString(stringNumber);
  }

  const returnValue = getSumOfEachDigit(stringNumber);

  return returnValue;
}

console.log(problem16(1000));
