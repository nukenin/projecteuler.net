/*
n! means n × (n − 1) × ... × 3 × 2 × 1

For example, 10! = 10 × 9 × ... × 3 × 2 × 1 = 3628800,
and the sum of the digits in the number 10! is 3 + 6 + 2 + 8 + 8 + 0 + 0 = 27.

Find the sum of the digits in the number 100!
*/

const stringMultiply = (stringNumber, multiple) => {
  // console.log('=======');
  let returnValue = '';
  let extra = 0;
  for (let i = stringNumber.length - 1; i >= 0; i -= 1) {
    let digit = stringNumber[i];
    digit = Number(digit) * multiple;
    if (extra) {
      digit += extra;
      extra = 0;
    }
    if (digit >= 10) {
      extra = Math.floor(digit / 10);
      digit = digit % 10;
    }
    digit = digit + '';
    returnValue = '' + digit + returnValue;
  }

  if (extra) {
    returnValue = '' + extra + returnValue;
  }

  // console.log(returnValue);

  return returnValue;


}

const problem20 = (n) => {
  let returnValue = 0;

  let stringNumber = '1';
  for(let i = 2; i <= n; i += 1) {
    stringNumber = stringMultiply(stringNumber, i);
    console.log(`${stringNumber} ${i}`);
  }

  for (let i = 0; i < stringNumber.length; i += 1) {
    returnValue += Number(stringNumber[i]);
  }

  return returnValue;
}

console.log(problem20(100));
