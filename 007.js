/*
By listing the first six prime numbers: 2, 3, 5, 7, 11, and 13, we can see that the 6th prime is 13.
What is the 10 001st prime number?
*/

const primeArray = [2];

// We have to keep a array of the all primes collected,
// and see if there are any remainders when we divide the numer in question
// by each of the primes.
// We take an early exit as soon as we find one.
const isPrime = (number) => {
  for (let a = 0; a < primeArray.length; a = a + 1) {
    const prime = primeArray[a];
    if (prime > Math.sqrt(number)) {
      return true;
    } else {
      if (number % prime === 0) {
        return false;
      } else {
        continue;
      }
    }
  }
  return true;
}

const problem7 = (nthPrime) => {
  let possiblePrime = 3;
  while(primeArray.length < nthPrime) {
    if (isPrime(possiblePrime)) {
      primeArray.push(possiblePrime);
    }
    // We can speed things up by adding 2 to it
    // because any even number is definitely not a prime.
    possiblePrime += 2;
  }
  return primeArray[primeArray.length - 1];
}

console.log(problem7(10001));