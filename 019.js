/*
You are given the following information, but you may prefer to do some research for yourself.

1 Jan 1900 was a Monday.
Thirty days has September,
April, June and November.
All the rest have thirty-one,
Saving February alone,
Which has twenty-eight, rain or shine.
And on leap years, twenty-nine.
A leap year occurs on any year evenly divisible by 4, but not on a century unless it is divisible by 400.
How many Sundays fell on the first of the month during the twentieth century (1 Jan 1901 to 31 Dec 2000)?
*/

const maxDays = [ 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];

const problem19 = () => {
  let returnValue = 0;

  // 1 based object to keep track of year, month, day, and day of week
  const date = {
    y: 1900,
    m: 1,
    d: 1,
    D: 1,
  };

  let track = false;

  while (!(date.y === 2001 && date.m === 1 && date.d === 1)) {
    console.log(`${date.y}/${date.m}/${date.d} ${date.D}`);
    if (date.y === 1901 && date.m === 1 && date.d === 1) {
      // We only want to keep tracking after Jan 1, 1901.
      track = true;
    }

    if (track && date.d === 1 && date.D === 7) {
      returnValue += 1;
    }
    
    let lastDayOfMonth = maxDays[date.m - 1];
    if (date.y % 4 === 0 && date.y % 100 != 0 && date.m === 2) {
      lastDayOfMonth = 29;
    }

    // Increment day
    date.d += 1;
    if (date.d > lastDayOfMonth) {
      date.m += 1;
      date.d = 1;
      if (date.m > 12) {
        date.y += 1;
        date.m = 1;
      } 
    }

    // Increment day of week
    date.D += 1;
    if (date.D > 7) {
      date.D = 1;
    }
  }

  return returnValue;
}

console.log(problem19());