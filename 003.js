/*
The prime factors of 13195 are 5, 7, 13 and 29.
What is the largest prime factor of the number 600851475143 ?
*/

var problem3 = function(original, number, prime, largest) {
  // The square root of the original times itself is the original,
  // therefore the largest prime can't be larger than the square root of the original
  if (number === 1 || largest >= Math.sqrt(original)) {
    return largest;
  } else if (number % prime === 0) {
    // If the number is divisible by the prime, then divide it and try the next higher number
    return problem3(original, number / prime, prime + 1, prime);
  } else {
    // If it's not divisible, then try the next higher number.
    return problem3(original, number, prime + 1, largest);
  }
}

console.log(problem3(600851475143, 600851475143, 2, 2));