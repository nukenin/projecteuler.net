/*
The sum of the squares of the first ten natural numbers is,
pow(1, 2) + pow(2, 2) + ... + pow(10, 2) = 385
The square of the sum of the first ten natural numbers is,
pow((1 + 2 + ... + 10), 2) = 552 = 3025
Hence the difference between the sum of the squares of the first ten natural numbers and the square of the sum is 3025 − 385 = 2640.
Find the difference between the sum of the squares of the first one hundred natural numbers and the square of the sum.
*/

/*
You can brute force it and the complexity is actually less than my solution,
assuming there is no penalty for calling pow().
It would be O(n) as you just need to loop through n twice to get the sums and then calculate the difference.
That would have been easier.

I actually over thought it below, and came up with a higher complexity of O(pow(n, 2) because I used math to figure out
a math equation. It's actually equal to a 2D matrix of n numbers where each cell is just a product and then
you sum it all up and except the squares.

It's because (1 + 2 ... n) squared = (1 + 2 + ... n)(1 + 2 + .. n)
which becomes 1 square + 2 squared + n square + 2 * (1 * 2) + 2 * (1 * 3) ... 
The squares cancel each other so it's essentially a sum of all the product of all the possible combinations
You have to times it by 2 because you add the row and columns 1 * 2 + 1 * 3 + 1 * 4 for the column
then you do it again but horizontally.

  |01 02 03 04 ...
------------------
01|01 02 03 04 ...
02|02 04 06 08 ...
03|03 06 09 12 ...
04|04 08 08 16 ...

However, maybe calling pow() has some complexity in assembly language.
At least in my solution, you don't need to call pow().

However, if you read the solutions in projecteuler.net forums, if you can figure out a formula for each sum
(eg, 385, and 3025), you make it a complexity of O(1)
*/
const problem6 = (n) => {
  let returnValue = 0;
  for (let a = 1; a <= n; a += 1) {
    for (let b = a + 1; b <= n; b += 1) {
      if (a == b) {
        continue;
      }
      returnValue += 2 * a * b;
    }
  }
  return returnValue;
}

console.log(problem6(100));