/*
2520 is the smallest number that can be divided by each of the numbers from 1 to 10 without any remainder.
What is the smallest positive number that is evenly divisible by all of the numbers from 1 to 20?
*/

const getPrimes = (max, index, primeArray) => {
  if (index === max) {
    return primeArray;
  } else {
    // If the array is empty, then stick the first index in there.
    // Cheating a bit here because we know the prime array will be empty
    // and we'll start with 2 for the index.
    if (primeArray.length === 0) {
      primeArray.push(index);
      return getPrimes(max, index + 1, primeArray);
    }

    let found = false;
    let a = index;
    for (let i = 0; i < primeArray.length; i += 1) {
      if (a % primeArray[i] === 0) {
        a = a / primeArray[i];
      }
    } 
    if (a !== 1) {
      primeArray.push(a);
    }
    return getPrimes(max, index + 1, primeArray)
  }

}

const problem5 = (n) => {
  let returnValue = 1;
  // If we were just to multiple all the numbers from 1 to 10,
  // we'd get some number but we know it can be smaller.
  // How do know it determine a smaller number?
  //
  // We break it down to their primes but for each higher prime we increment to,
  // We check if it can be divisible by any of them once, and just keep what is left.
  // 
  // For example, as we go from 2 to 10,
  // We will collect:
  // [ 2, 3, 2, 5, 7, 2, 3 ]
  // At 4, 4 is divisible by 2, and therefore we keep 2.
  // When we get to 6, 6 is divisible by 2 and 3 preceding it.
  // Therefore, we do not keep 6.
  // When we get to 8, there are two 2's before it, therefore we keep 2.
  // When we get to 9, divide it by 3, and keep 3.
  // The final product is 2 * 3 * 2 * 5 * 7 * 2 * 3 = 2520 
  const primeList = getPrimes (n, 2, []);
  console.log(primeList);
  // Then multiple all of them together to get the answer.
  for (let i = 0; i < primeList.length; i++) {
    returnValue *= primeList[i];
  }
  return returnValue;
}

console.log(problem5(20));