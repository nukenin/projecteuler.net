/*
A Pythagorean triplet is a set of three natural numbers, a < b < c, for which,
a2 + b2 = c2
For example, 32 + 42 = 9 + 16 = 25 = 52.
There exists exactly one Pythagorean triplet for which a + b + c = 1000.
Find the product abc.
*/

const problem9Brute = (sum) => {
  let returnValue = -1;

  // We save all the numbers that are squares to save us some cycles in the loop
  // It is O(n) at this point.
  const squares = {}
  for (let i = 1; i < sum; i += 1) {
    squares[i * i] = true;
  }

  // This will add an O(n2) making is O(n + n2).
  // This is very bad LOL.
  for (let b = 1; b < sum; b += 1) {
    for (let a = 1; a < b; a += 1) {
      const a2 = a * a;
      const b2 = b * b;
      const c2 = a2 + b2;
      // Check if the sum's square root is an integer.
      // to save some cycles.
      if (squares[c2]) {
        const c = Math.sqrt(c2);
        if (a + b + c === sum) {
          returnValue = a * b * c;
          break;
        }
      }
    }
    if (returnValue > -1) {
      break;
    }
  }

  return returnValue;
}

// I sorta knew we needed some more equation to solve this not by brute force.
// Based on Pythagorean proof where a = 2mn, b = m^2 - n^2, c = m^2 + n^2
// which I found in the forums and via the web to find Pythagorean triplets.
// We simplify the equation where:
// 1000 = 2mn + m^2 - n^2 + m^2 + n^2.
// 1000 = 2mn + 2m^2
// 500 = mn + m^2
// n = (500 - m^2) / m
// Find n where (500 - m^2) / m where n is an integer and m > n 
// m has to be less than square root of 500 so we will start there.
// It is O(sqrt(n)) which is pretty awesome.
const problem9Optimized = (sum) => {
  let returnValue = -1;
  const max = Math.floor(Math.sqrt(sum / 2));
  for(let m = max; m >= 0; m += -1) {
    const n = (500 - m * m) / m;
    if (Number.isInteger(n) && m > n) {
      returnValue = 2 * m * n * (m * m - n * n) * (m * m + n * n);
      break;
    }
  }

  return returnValue;
}


console.log(problem9Optimized(1000));