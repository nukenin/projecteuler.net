/*
Starting in the top left corner of a 2×2 grid, and only being able to move to the right and down, there are exactly 6 routes to the bottom right corner.

How many such routes are there through a 20×20 grid?
*/

const cache = [];

const problem15 = (x, y, max) => {
  // console.log(`${x} ${y}`)
  let returnValue = 1;
  if (x >= max && y >= max) {
    return returnValue;
  }

  if (!cache[x]) {
    cache[x] = [];
  }

  if (cache[x][y]) {
    returnValue = cache[x][y];
  } else if (x >= max) {
    returnValue = problem15(x, y + 1, max);
    cache[x][y + 1] = returnValue;
  } else if (y >= max) {
    returnValue = problem15(x + 1, y, max);
    cache[x + 1][y] = returnValue;
  } else {
    const returnValue1 = problem15(x + 1, y, max);
    const returnValue2 = problem15(x, y + 1, max)
    cache[x + 1][y] = returnValue1;
    cache[x][y + 1] = returnValue2
    returnValue = returnValue1 + returnValue2;
  }

  return returnValue;
}

console.log(problem15(0, 0, 20, 0));
