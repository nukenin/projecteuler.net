/*
If we list all the natural numbers below 10 that are multiples of 3 or 5, we get 3, 5, 6 and 9. The sum of these multiples is 23.
Find the sum of all the multiples of 3 or 5 below 1000.
*/

var problem1 = function(sum, multiple, baseMultiple, max) {
  if (multiple >= max) {
    return sum;
  } else {
    return problem1(sum + multiple, multiple + baseMultiple, baseMultiple, max);
  }
}

// We basically recursively sum up all the multiple of 3, add it with the multiple of 5,
// but then subtract the duplicates where the number is a mutiple of both, ie 15.
console.log(problem1(problem1(0, 3, 3, 1000), 5, 5, 1000) - problem1(0, 15, 15, 1000));