/*
The sequence of triangle numbers is generated by adding the natural numbers. So the 7th triangle number would be 1 + 2 + 3 + 4 + 5 + 6 + 7 = 28. The first ten terms would be:

1, 3, 6, 10, 15, 21, 28, 36, 45, 55, ...

Let us list the factors of the first seven triangle numbers:

 1: 1
 3: 1,3
 6: 1,2,3,6
10: 1,2,5,10
15: 1,3,5,15
21: 1,3,7,21
28: 1,2,4,7,14,28
We can see that 28 is the first triangle number to have over five divisors.

What is the value of the first triangle number to have over five hundred divisors?
*/

const getDivisorCount = (numberParameter) => {
  /* A fast algorithm from the forums of projecteuler.net
  num := 0;
  j := 0;
  repeat
  inc(j);
  f := 0;
  num := num + j;
    for n := 1 to round(sqrt(num)) do
    if num mod n = 0 then
    f := f + 2;
  until
  f > 500;
  canvas.textout(10,50,inttostr(num));
  */
 let returnValue = 0;
  // This works because we are simply finding all the numbers
  // that are divisors up to the square root of the number.
  // If it divides with no remainder, it means there are 2 numbers
  // that can be divided thus we add 2.
  // Brilliant!
  // Runs much faster than your original algorithm
  // because we don't need to keep track of the prime or what
  // the divisor is.
  for (let n = 1; n * n < numberParameter; n += 1) {
    if (numberParameter % n == 0) {
      returnValue += 2;
    }
  }
  return returnValue;
}

const getDivisorCount2 = (numberParameter) => {
  const primes = {};
  let returnValue = 1;
  let number = numberParameter;
  let divisor = 2;
  let done = false;
  while (!done && divisor < numberParameter) {
    if (number % divisor === 0) {
      if (primes[divisor] === undefined) {
        primes[divisor] = 1;
      } else {
        primes[divisor] += 1;
      }
      number = number / divisor;
      if (divisor * divisor >= numberParameter) {
        done = true;
      }
    } else {
      divisor += 1;
    }
  }

  // console.log(primes);

  for(prime in primes) {
    returnValue *= primes[prime] + 1;
  }
  return returnValue;
}

const problem12 = (minDivisors) => {
  let divisorCount = 0;
  let naturalNumber = 0;
  let sum = 0;
  while(divisorCount < minDivisors) {
    naturalNumber += 1;
    sum += naturalNumber; 
    divisorCount = getDivisorCount(sum);
    console.log(`${naturalNumber} has a triangle of ${sum} has ${divisorCount}`);
  }
  return `${sum} is first triangle with more than ${minDivisors} divisors.`;
}

console.log(problem12(500))