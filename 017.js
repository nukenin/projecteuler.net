/*
If the numbers 1 to 5 are written out in words: one, two, three, four, five, then there are 3 + 3 + 5 + 4 + 4 = 19 letters used in total.

If all the numbers from 1 to 1000 (one thousand) inclusive were written out in words, how many letters would be used?


NOTE: Do not count spaces or hyphens. For example, 342 (three hundred and forty-two) contains 23 letters and 115 (one hundred and fifteen) contains 20 letters. The use of "and" when writing out numbers is in compliance with British usage.
*/

const dictionary = {
  0: '',
  1: 'one',
  2: 'two',
  3: 'three',
  4: 'four',
  5: 'five',
  6: 'six',
  7: 'seven',
  8: 'eight',
  9: 'nine',
  10: 'ten',
  11: 'eleven',
  12: 'twelve',
  13: 'thirteen',
  14: 'fourteen',
  15: 'fifteen',
  16: 'sixteen',
  17: 'seventeen',
  18: 'eighteen',
  19: 'nineteen',
  20: 'twenty',
  30: 'thirty',
  40: 'fourty',
  50: 'fifty',
  60: 'sixty',
  70: 'seventy',
  80: 'eighty',
  90: 'ninty',
  100: 'onehundred',
  1000: 'onethousand',
}

const problem17 = () => {
  let returnValue = 0;
  let length = 0;
  for (let i = 1; i <= 1000; i += 1) {
    if (dictionary[i]) {
      length = dictionary[i].length;
    } else if (i <= 100) {
      dictionary[i] = dictionary[Math.floor(i / 10) * 10] + dictionary[i % 10];
      length = dictionary[i].length;
    } else if (i <= 1000) {
      dictionary[i] = dictionary[Math.floor(i / 100)] + 'hundred';
      const remainder = i % 100;
      if (remainder > 0) {
        dictionary[i] += 'and' + dictionary[remainder];
      }
      length = dictionary[i].length;
    } else {
      length = 0;
    }

    returnValue += length;
  }

  console.log(dictionary);

  return returnValue;
}

console.log(problem17());
