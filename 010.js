/*
The sum of the primes below 10 is 2 + 3 + 5 + 7 = 17.
Find the sum of all the primes below two million.
*/

/* 
Easiest way is to start low and mark all multiples starting from 2.
Then find the next unmarked integer and mark all multiples of those, ie. 3.
Then find multiples of 5, then 7, etc.
This means we are basically testing which is not a prime.
How to implement efficiently is the key.
*/

const primesArray = [2];

// This is O(m*n) because we have to loop through to the number below 2 million
// and then we also have to loop through all the primes in the primes array.
const problem10Brute = (max) => {
  for (let primeCandidate = 3; primeCandidate <= max; primeCandidate += 1) {
    let isPrime = true;
    // Like in previous solutions, we keep track of all the primes,
    // and see if the remainer of any candidate is 0.
    for (let i = 0; i < primesArray.length; i += 1) {
      if (primeCandidate % primesArray[i] === 0) {
        isPrime = false;
        break;
      }
    }
    if (!isPrime) {
      continue;
    }
    primesArray.push(primeCandidate);
  }
  let returnValue = 0;
  for (let i = 0; i < primesArray.length; i += 1) {
    returnValue += primesArray[i];
  }
  return returnValue;
}

const possiblePrime = {};

// This is more efficiently because it is O(n).
// 
const problem10 = (max) => {
  // We start with 2 and fill the map with all possible untested primes.
  // We just mark it with true so a value is saved in the map.
  for(let i = 2; i <= max; i += 1) {
    possiblePrime[i] = true;
  }

  let returnValue = 0;
  for(let primeCandidate = 2; primeCandidate <= max; primeCandidate += 1) {
    // If the candidate does not exist in the map, it means it is already tested
    // and is not a prime. Therefore, we can skip it.
    if (!possiblePrime[primeCandidate]) {
      continue;
    }
    
    // At this point, the candidate is a prime. Therefore we add it to the sum.
    returnValue += primeCandidate;

    // Now for all multiples of that candidate, remove it from the possible prime map.
    let multiple = primeCandidate + primeCandidate;
    while(multiple <= max) {
      delete possiblePrime[multiple];
      multiple += primeCandidate;
    }
  }
  return returnValue;
}

console.log(problem10(2000000));
// Answer: 142913828922