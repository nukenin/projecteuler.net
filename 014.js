/*
The following iterative sequence is defined for the set of positive integers:

n → n/2 (n is even)
n → 3n + 1 (n is odd)

Using the rule above and starting with 13, we generate the following sequence:

13 → 40 → 20 → 10 → 5 → 16 → 8 → 4 → 2 → 1
It can be seen that this sequence (starting at 13 and finishing at 1) contains 10 terms. Although it has not been proved yet (Collatz Problem), it is thought that all starting numbers finish at 1.

Which starting number, under one million, produces the longest chain?

NOTE: Once the chain starts the terms are allowed to go above one million.
*/

const problem14 = () => {
  /*
  Eventually we will hit a power of 2 and it will result in 1.
  So let's start with that and work backwards
  and find a number that is multiple of 3 that is 1 less than the power of 2.
  But what is the point of this exercise?
  Well the point of the 3n + 1 is to make it even,
  and increase the chance of it being broken down.
  So I guess the point is to find the number under 1 million that is the hardest to break down.
  We can brute force it too and start from 999,999
  */

  const cache = {}

  const getChainCount = (original, n, chainCount) => {
    // console.log('====================');
    // console.log(`${original} ${n} ${chainCount}`);
    if (cache[n]) {
      cache[original] = cache[n] + chainCount;
      return cache[original];
    }

    if (n == 1) {
      cache[original] = chainCount + 1;
      return cache[original];
    } else if (n % 2 == 0) {
      return getChainCount(original, n/2, ++chainCount);
    } else {
      return getChainCount(original, n*3 + 1, ++chainCount);
    }
  }

  let maxChain = 0;
  let start = 0;

  for (let n = 1; n <= 999999; n += 1) {
    const chainCount = getChainCount(n, n, 0);
    if (chainCount > maxChain) {
      start = n;
      maxChain = chainCount;
    }
  }
  return start;
}

console.log(problem14());